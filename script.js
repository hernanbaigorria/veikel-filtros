$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div style="margin-bottom:10px;"> <select name="select_01[' + x + ']"><option>Filtros</option></select> <select name="select_02[' + x + ']"><option>Reglas</option></select> <input type="text" name="filtro[' + x + ']"/> <a href="#" class="remove_field btn btn-warning" style="color: #fff;font-size: 11px;margin: 0 10px;position: relative;top: -3px;">Remove</a> </div>'); // add input boxes.
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });

    $( ".ajustes-avanzados" ).click(function() {
      $('.content-more-filter-avanced').toggleClass("show-content");
    });
});